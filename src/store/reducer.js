import { combineReducers } from 'redux';
import { app, streams } from '../reducers';

export default combineReducers({
  app,
  streams
});
