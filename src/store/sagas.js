import {
    helloSaga,
    fetchWatcher,
    streamsWatcher
  } from '../sagas';
  
  // single entry point to start all Sagas at once
  export default function* sagas() {
    yield [
      helloSaga(),
      fetchWatcher(),
      streamsWatcher(),
    ];
  }
  