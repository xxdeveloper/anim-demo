import Polyglot from 'node-polyglot';

const polyglot = new Polyglot();

polyglot.extend({
  user: {
    hi: 'Hi!',
    hello: 'Hello, %{value_0}!',
    search: 'Search...',
    my_settings: 'Settings',
    logout: "Logout"
  },
  menu: {
    home: 'Home',
    top: 'Top Videos',
    trailers: 'Trailers',
    music: 'Music',
    education: 'Education',
    games: 'Games'
  },
  social: {
    requests: 'Requests',
    messages: 'Messages',
    notifications: 'Notifications'
  },
  common: {
    copyrights: 'Copyright © 2016-2017. Evgenii Torgonin.'
  }
});

export default polyglot;
