import Polyglot from 'node-polyglot';

const polyglot = new Polyglot();

polyglot.extend({
  user: {
    hi: 'Привет!',
    hello: 'Привет, %{value_0}!',
    search: 'Поиск...',
    my_settings: 'Настройки',
    logout: "Выход"
  },
  menu: {
    home: 'Главная',
    top: 'Популярное',
    trailers: 'Трейлеры',
    music: 'Музыка',
    education: 'Образование',
    games: 'Игры'
  },
  social: {
    requests: 'Запросы',
    messages: 'Сообщения',
    notifications: 'Уведомления'
  },
  common: {
    copyrights: 'Copyright © 2016-2017. Evgenii Torgonin.'
  }
});

export default polyglot;
