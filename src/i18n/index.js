/* eslint-disable no-undef */
if (process.env.LANG === 'en') {
    // eslint-disable-next-line global-require
    module.exports = require('./en');
  } else {
    // eslint-disable-next-line global-require
    module.exports = require('./ru');
  }
  