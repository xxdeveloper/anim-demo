import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3333';
//axios.defaults.baseURL = 'http://52.166.146.86';
axios.defaults.timeout = 5000;
axios.defaults.headers.post['Content-Type'] = 'application/json';

export default axios;
