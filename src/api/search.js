import axios from './axios-config';

const search = {
  fetchUrl(url) {
    return axios.post('/api/search/fetch_url', {
      url
    });
  }
};

export default search;
