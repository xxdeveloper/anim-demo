import search from './search';
import streams from './streams';

const api = {
  search,
  streams
};

export default api;
