import axios from './axios-config';

const streams = {
  getTop(url) {
    return axios.get(`api/feed/${url}`);
  },
  channel(cid) {
    return axios.get('api/channel', { cid });
  },
};

export default streams;
