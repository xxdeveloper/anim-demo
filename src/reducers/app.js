import { ENV } from '../actions';

const initialState = {
  version: '1.0.0a',
  loading: true,
  isMobile: false,
  window: {
    width: 1920,
    height: 1080
  }
};

export function app(state = initialState, action) {
  switch (action.type) {
    case ENV.INIT_DONE:
      return {
        ...state,
        loading: false
      };
    case ENV.MOBILE:
      return {
        ...state,
        isMobile: action.isMobile
      };
    case ENV.RESIZE:
      return {
        ...state,
        window: {
          width: action.width,
          height: action.height
        }
      };
    default:
      return state;
  }
}
