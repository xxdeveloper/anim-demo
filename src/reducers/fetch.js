import { FETCH } from '../actions';

const initialState = {
  errors: null,
  fetchUrl: {
    isLoading: false,
    data: null
  }
};

export function fetchUrl(state = initialState, action) {
  switch (action.type) {
    case FETCH.URL_REQUEST:
      return {
        ...state,
        fetchUrl: {
          isPending: true,
          data: null
        }
      };
    case FETCH.URL_RECEIVE:
      return {
        ...state,
        fetchUrl: {
          isPending: false,
          data: action.data
        }
      };
    default:
      return state;
  }
}
