import { STREAMS } from "../actions";

const initialState = {
  errors: null,
  list: {
    isPending: false,
    data: []
  },
  channel: {
    isPending: false,
    source: "",
    data: null
  }
};

export function streams(state = initialState, action) {
  switch (action.type) {
    case STREAMS.VIDEOS_REQUEST:
      return {
        ...state,
        list: {
          isPending: true,
          data: []
        }
      };
    case STREAMS.VIDEOS_RECEIVE:
      return {
        ...state,
        list: {
          isPending: false,
          data: action.streams
        }
      };
    case STREAMS.OPEN_PLAYER:
      return {
        ...state,
        channel: {
          isPending: false,
          source: action.source,
          data: action.channel
        }
      };
    case STREAMS.CLOSE_PLAYER:
      return {
        ...state,
        channel: {
          isPending: false,
          data: null
        }
      };
    default:
      return state;
  }
}
