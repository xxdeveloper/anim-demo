import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import polyglot from "../../i18n";

const MenuLink = ({ link }) => (
  <li>
    <NavLink to={link.to} exact={link.exact} activeClassName="text-pink">
      <i className={link.icon} />
      <span>{link.text}</span>
    </NavLink>
  </li>
);

class Sidebar extends Component {
  render() {
    const links = [
      { id: 1, to: "/", icon: "fa fa-home fa-fw", text: polyglot.t('menu.home'), exact: true },
      { id: 2, to: "/youtube-top", icon: "fa fa-fire fa-fw", text: polyglot.t('menu.top') },
      { id: 3, to: "/youtube-trailers", icon: "fa fa-film fa-fw", text: polyglot.t('menu.trailers') },
      { id: 4, to: "/youtube-music", icon: "fa fa-music fa-fw", text: polyglot.t('menu.music') },
      { id: 5, to: "/youtube-education", icon: "fa fa-graduation-cap fa-fw", text: polyglot.t('menu.education') },
      { id: 6, to: "/twitch-top", icon: "fa fa-gamepad fa-fw", text: polyglot.t('menu.games') },
    ];

    const list = links.map(link => <MenuLink key={link.id} link={link} />);

    return (
      <div className="sidebar sidebar-main sidebar-default sidebar-fixed visible-lg-block">
        <div className="sidebar-content">
          {/*Main navigation menu*/}
          <div className="sidebar-category sidebar-category-visible">
            <div className="category-content no-padding">
              <ul className="navigation navigation-main navigation-accordion">
                {list}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Sidebar;
