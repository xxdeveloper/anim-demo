import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Header from '../Header';
import Sidebar from '../Sidebar';

class DefaultLayout extends Component {
  render() {
    const { component: Component, children, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={matchProps =>
          <div className="App">
            <Header />
            <Component {...matchProps} />
          </div>}
      />
    );
  }
}

class FrontendLayout extends Component {
  render() {
    const { component: Component, children, ...rest } = this.props;
    return (
      <DefaultLayout
        {...rest}
        component={matchProps =>
          <div className="page-container">
            <div className="page-content">
              <Sidebar />
              <div className="content-wrapper">
                {children}
              </div>
            </div>
          </div>}
      />
    );
  }
}

export class SidebarLayout extends Component {
  render() {
    const { component: Component, children, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={matchProps =>
          <div className="page-container">
            <div className="page-content">
              <Component {...matchProps} />
              <div className="content-wrapper">
                {children}
              </div>
            </div>
          </div>}
      />
    );
  }
}

class Layout extends Component {
  render() {
    return (
      <div className="page-container">
        <div className="page-content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Layout;
