import React, { Component } from "react";
import { connect } from "react-redux";
import { streams } from "../../actions";
import StreamsList from "./components/StreamsList";
import Spinner from "./components/Spinner";

class Category extends Component {
  constructor(props) {
    super(props);

    props.fetchList(props.type || "twitch");

    this.onItem = this.onItem.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.type !== this.props.type) {
      this.props.fetchList(nextProps.type || "twitch");
    }
  }

  onItem = id => this.props.changeCurrentStream(this.props.type.split("-")[0], id);

  onScroll = () => this.props.fetchStreams(this.props.streams, "top");

  render() {
    const { pending, data } = this.props;
    const component = pending ? (
      <Spinner />
    ) : (
      <StreamsList streams={data} onStream={this.onItem} onScroll={this.onScroll} />
    );
    return <div className="container text-center col-md-10">{component}</div>;
  }
}

const mapStateToProps = store => ({
  data: store.streams.list.data,
  pending: store.streams.list.isPending
});

const mapDispatchToProps = dispatch => ({
  changeCurrentStream: (source, channel) => {
    const link = source === "twitch"
        ? `https://player.twitch.tv/?channel=${channel.name}`
        : `//www.youtube.com/embed/${channel.url}?autoplay=1`;

    dispatch(streams.openPlayer(source, link));
  },
  fetchList: category => dispatch(streams.requestVideosList(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(Category);
