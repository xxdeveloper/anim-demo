import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import numeral from "numeral"

class StreamsItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      hover: false,
      hideClass: 'hide',
    };
  }

  componentDidMount() {
    let domNode = ReactDOM.findDOMNode(this.node);

    const deltaX = 3000;
    const deltaY = 0;

    let time = (this.props.id % 4) * 200 + Math.random() * 200 + 500;

    requestAnimationFrame(() => {
      domNode.style.transform = `translate(${deltaX}px, ${deltaY}px)`;
      domNode.style.transition = "transform 0s";
      this.setState({hideClass: ''});

      requestAnimationFrame(() => {
        domNode.style.transform = "none";
        domNode.style.transition = `transform ${time}ms`;
      });
    });
  }

  mouseOver = () => this.setState({ hover: true });

  mouseOut = () => this.setState({ hover: false });

  render() {
    const { id, stream, onClick } = this.props;
    const {channel, viewers} = stream;
    const layout = `grid-default ${this.state.hideClass}`;

    return (
      <div
        ref={item => {
          this.node = item;
        }}
        className={`${layout} col-xs-12 col-sm-6 col-md-4 col-lg-4`}
        onMouseOver={this.mouseOver.bind(this)}
        onMouseOut={this.mouseOut.bind(this)}
      >
        <div className="thumbnail cursor-pointer" onClick={onClick}>
          <div className={`thumb border-bottom-lg ${BottomClass(id)}`}>
            <div className="video-header">
              <span className="label label-rounded label-icon pull-left">
                <i className="icon-play" />
              </span>
              <span className="label label-rounded label-icon pull-right">
                <i className="icon-volume-mute2" />
              </span>
            </div>

            <img src={stream.preview.large} alt="stream preview" />

            <div className="video-stats clearfix">
              <div className="thumb-stats pull-left">
                <h6 className="no-margin text-left">
                  {PrintTitle(stream.game)}
                  <small className="display-block">
                    <span className="text-semibold">by {channel.name}</span>
                  </small>
                </h6>
              </div>
              <div className="thumb-stats pull-right">
                <i className="icon-eye2" />
                <span>{viewers ? numeral(viewers).format('0.0a') : ''}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default StreamsItem;

const PrintTitle = str => (str.length > 28 ? `${str.substr(0, 25)}...` : str);

function BottomClass(id) {
  const rnd = id % 4;
  let color;
  switch (rnd) {
    case 1:
      color = "border-bottom-primary";
      break;
    case 2:
      color = "border-bottom-warning";
      break;
    case 3:
      color = "border-bottom-success";
      break;
    default:
      color = "border-bottom-danger";
      break;
  }

  return color;
}
