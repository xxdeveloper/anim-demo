import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import $ from "jquery";

class VideoModal extends PureComponent {
  componentDidMount() {
    $(ReactDOM.findDOMNode(this)).modal("show");
    $(ReactDOM.findDOMNode(this)).on(
      "hidden.bs.modal",
      this.props.handleHideModal
    );
  }

  render() {
    const { src, height, onClose } = this.props;
    return (
      <div className="modal stream-modal fade">
        <div className="modal-dialog modal-full">
          <div className="modal-content bg-grey-800">
            <button
              type="button"
              className="btn btn-primary btn-icon btn-rounded modal-close"
              onClick={onClose}
              data-dismiss="modal"
              aria-hidden="true"
            >
              <i className="fa fa-times fa-fw" />
            </button>
            <div className="modal-body">
              <iframe
                title="video"
                src={src}
                height={height-70}
                width="100%"
                frameBorder="0"
                scrolling="no"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VideoModal;
