import React, { PureComponent } from 'react';
import StreamsItem from './StreamsItem';
import infiniteScroll from './InfiniteScroll';

class StreamsList extends PureComponent {
  render() {
    const { streams, onStream } = this.props;
    return (
      <div className="streams-container row">
        {streams.map((stream, i) => {
          const onClick = onStream.bind(this, stream.channel);
          return (
            <StreamsItem id={i} stream={stream} onClick={onClick} key={i} />
          );
        })}
      </div>
    );
  }
}

export default infiniteScroll(StreamsList);
