import React, { PureComponent } from 'react';

class CategoriesItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      shaked: null,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active !== this.props.active) {
      this.mouseOut();
    }
  }

  mouseOver() {
    if (!this.props.active) {
      this.setState({ hover: true });
    }
  }

  mouseOut = ()  => this.setState({ hover: false });

  render() {
    const color = this.state.hover ? this.props.color : '#ffffff';
    const active = ""; //this.props.active ? "shaked-icon" : "";
    const background = this.state.hover ? '#ffffff' : this.props.color;
    return (
      <div
        className="panel panel-body text-center"
        style={{ background, height: '100%' }}
        onMouseOver={this.mouseOver.bind(this)}
        onMouseOut={this.mouseOut.bind(this)}
      >
        <div className="svg-center position-relative" id="progress_icon_four">
          <i
            className={ `${this.props.icon} ${active}` }
            style={{
              color,
              top: 16 + 'px',
              width: 84 + 'px',
              height: 84 + 'px'
            }}
          />
        </div>
        <div className="text-size-small opacity-75" style={{ color }}>
          {this.props.title}
        </div>
      </div>
    );
  }
}

export default CategoriesItem;
