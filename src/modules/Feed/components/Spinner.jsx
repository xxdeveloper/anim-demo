import React, { PureComponent } from 'react';

class Spinner extends PureComponent {
  render() {
    return (
      <div className="cs-loader">
        <div className="cs-loader-inner">
          <label className="text-primary"> ●</label>
          <label className="text-primary"> ●</label>
          <label className="text-primary"> ●</label>
          <label className="text-primary"> ●</label>
          <label className="text-primary"> ●</label>
          <label className="text-primary"> ●</label>
        </div>
      </div>
    );
  }
}

export default Spinner;
