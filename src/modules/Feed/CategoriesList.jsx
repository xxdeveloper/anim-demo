import React, { Component } from "react";
import ReactDOM from "react-dom";
import CategoriesItem from "./components/CategoriesItem";
import { Link } from "react-router-dom";
import Welcome from "./components/Welcome";

class CategoriesList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      boxes: {}
    };
  }

  shouldComponentUpdate = (nextProps, nextState) => this.props.active !== nextProps.active;

  componentWillReceiveProps(nextProps) {
    let boxes = {};

    nextProps.list.forEach((child, i) => {
      const domNode = ReactDOM.findDOMNode(this[child.id]);
      if (domNode) {
        boxes[child.id] = domNode.getBoundingClientRect();
      }
    });
    this.setState({ boxes });
  }

  componentDidUpdate(previousProps, prevState) {
    this.props.list.forEach((child, i) => {
      const domNode = ReactDOM.findDOMNode(this[child.id]);
      domNode.style.opacity = 0;

      let newBox = domNode.getBoundingClientRect();
      const oldBox = this.state.boxes[child.id]
        ? this.state.boxes[child.id]
        : newBox;

      let scale =
        oldBox.height === newBox.height
          ? oldBox.height / document.body.offsetHeight
          : oldBox.height / newBox.height;

      let deltaX = oldBox.left - newBox.left;
      let deltaY = oldBox.top - newBox.top;

      requestAnimationFrame(() => {
        domNode.style.transformOrigin = `50% 0`;
        domNode.style.transform = `matrix(1, 0, 0, ${scale}, ${deltaX}, ${deltaY})`;
        domNode.style.transition = "transform 0s";

        requestAnimationFrame(() => {
          domNode.style.opacity = 1;
          domNode.style.transform = "none";
          domNode.style.transition = "transform .4s";
        });
      });
    }, this);
  }

  isVisible(item, active) {
    return active === null
      ? null
      : item.id !== active ? { display: "none" } :  { height: "100%" };
  }

  renderLinks() {
    const active = this.props.active;
    const itemsize = active ? "" : "col-md-2";

    let map = this.props.list.map((title, i) => {
      const itemstyle = this.isVisible(title, active);
      const link = active === title.id ? "/" : `/${title.id}`;
      return (
        <div
          ref={item => (this[title.id] = item)}
          key={title.id}
          className={`category-item ${itemsize}`}
          style={itemstyle}
        >
          <Link to={link}>
            <CategoriesItem
              active={title.id === active}
              title={title.title}
              icon={title.icon}
              color={title.color}
              overview={title.overview}
            />
          </Link>
        </div>
      );
    }, this);
    return map;
  }

  render() {
    const colsize = this.props.active ? "col-md-2" : "col-md-12 col-xs-12";
    const welcome = this.props.active ? null : <Welcome />; 
    let titles = this.renderLinks();
    return (
      <div className={`titles-wrapper ${colsize}`}>
        <div className="col-md-1" />
        {titles}
        <div className="col-md-1" />
        <div className="col-md-12">
          {welcome}
        </div>
      </div>
    );
  }
}

export default CategoriesList;
