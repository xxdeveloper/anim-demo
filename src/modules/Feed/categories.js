import polyglot from "../../i18n";

export const categories = [
    {
      id: "youtube-top",
      title: polyglot.t('menu.top'),
      icon: "fa fa-fire fa-4x",
      overview: "overview",
      color: "rgb(0, 146, 246)",
      backdrop: "backdrop"
    },
    {
      id: "youtube-trailers",
      title: polyglot.t('menu.trailers'),
      icon: "fa fa-film fa-4x",
      overview: "overview",
      color: "rgb(255, 0, 0)",
      backdrop: "backdrop"
    },
    {
      id: "youtube-music",
      title: polyglot.t('menu.music'),
      icon: "fa fa-music fa-4x",
      overview: "overview",
      color: "rgb(0, 146, 246)",
      backdrop: "backdrop"
    },
    {
      id: "youtube-education",
      title: polyglot.t('menu.education'),
      icon: "fa fa-graduation-cap fa-4x",
      overview: "overview",
      color: "rgb(0, 184, 96)",
      backdrop: "backdrop"
    },
    {
      id: "twitch-top",
      title: polyglot.t('menu.games'),
      icon: "fa fa-gamepad fa-4x",
      overview: "overview",
      color: "rgb(127, 95, 203)",
      backdrop: "backdrop"
    }
  ];
  