import React, { PureComponent } from "react";
import { Route, withRouter } from "react-router-dom";
import { categories } from "./categories";
import CategoriesList from "./CategoriesList";
import Category from "./Category";

class Feed extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      active: this.getActiveFolder(props.location)
    };
  }

  componentWillReceiveProps = nextProps => {
    this.changeActive(nextProps.location);
  };

  changeActive = location => {
    const active = this.getActiveFolder(location);
    this.setState({ active });
  };

  getActiveFolder(location) {
    let pathArr = location
      ? location.pathname.split("/")
      : this.props.location.pathname.split("/");
  
    let result = null;
  
    if (pathArr.length > 1) {
      categories.forEach(function(item, i) {
        if (item.id === pathArr[pathArr.length - 1]) {
          result = pathArr[pathArr.length - 1];
        }
      });
    }
    return result;
  }

  render() {
    const { active } = this.state;
    const subroute = active ? (
      <Route
        path={`${this.props.match.url}${active}`}
        render={props => <Category {...props} type={active} />}
      />
    ) : null;

    return (
      <div className="feed-row">
        <CategoriesList active={this.state.active} list={categories} />
        {subroute}
      </div>
    );
  }
}

export default withRouter(Feed);
