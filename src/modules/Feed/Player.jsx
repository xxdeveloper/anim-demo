import React, { PureComponent } from "react";
import VideoModal from "./components/VideoModal";
import { connect } from "react-redux";
import { streams } from "../../actions";

class Player extends PureComponent {
  render() {
    const { height, channel, source, onClose } = this.props;

    const isShow = channel ? (
      <VideoModal onClose={onClose} src={channel} source={source} height={height} />
    ) : null;
    return isShow;
  }
}

const mapStateToProps = store => ({
  height: store.app.window.height,
  isMobile: store.app.isMobile,
  source: store.streams.channel.source,
  channel: store.streams.channel.data
});

const mapDispatchToProps = dispatch => ({
  onClose: () => dispatch(streams.closePlayer())
});

export default connect(mapStateToProps, mapDispatchToProps)(Player);
