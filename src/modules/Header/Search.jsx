import React from 'react';
import polyglot from "../../i18n";

const Search = props =>
  <form className="navbar-form Search" role="search" onSubmit={()=>{alert('Dummy element...')}}>
    <input type="search" placeholder={polyglot.t('user.search')} />
  </form>;

export default Search;
