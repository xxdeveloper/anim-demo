import React, { Component } from "react";
import { Link } from "react-router-dom";
import polyglot from "../../i18n";

class UserMenu extends Component {
  render() {
    const user = this.props.user;

    const settings = [
      {
        id: 1,
        to: "/",
        icon: "fa fa-cogs fa-fw",
        text: polyglot.t("user.my_settings")
      },
      {
        id: 2,
        to: "/",
        icon: "fa fa-power-off fa-fw",
        text: polyglot.t("user.logout"),
        onClick: this.props.onLogout
      }
    ];

    const list_settings = settings.map(link => (
      <li key={link.id}>
        <Link to={link.to} onClick={link.onClick}>
          <i className={link.icon} />
          <span>{link.text}</span>
        </Link>
      </li>
    ));

    const avatar = user.avatar || "http://lorempixel.com/200/200/";
    const name = user.name || "TestUser";

    return (
      <li className="dropdown dropdown-user">
        <a className="dropdown-toggle" data-toggle="dropdown">
          <img src={avatar} alt="user avatar" />
          <span>{name}</span>
          <i className="fa fa-angle-down position-right" />
        </a>
        <ul className="dropdown-menu dropdown-menu-right">
          {list_settings}
        </ul>
      </li>
    );
  }
}

export default UserMenu;
