import React, { PureComponent } from 'react';

class MobileHeader extends PureComponent {
  render() {
    // const { targetMenu, targetUser, targetGrid } = this.props;
    return (
      <ul className="nav navbar-nav pull-right visible-xs-block">
        <li>
          <a data-toggle="collapse" data-target="#navbar-mobile">
            <i className="icon-paragraph-justify3" />
          </a>
        </li>
        <li>
          <a className="sidebar-mobile-main-toggle">
            <i className="icon-paragraph-justify3" />
          </a>
        </li>
        <li>
          <a className="sidebar-mobile-opposite-fix">
            <i className="icon-grid7" />
          </a>
        </li>
      </ul>
    );
  }
}

export default MobileHeader;
