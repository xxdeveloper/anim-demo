import React, { PureComponent } from 'react';
import polyglot from '../../../i18n';

let icons = {
  notifications: 'fa fa-bell',
  requests: 'fa fa-flag',
  messages: 'fa fa-envelope'
};

class NotifyDropdown extends PureComponent {
  constructor(props) {
    super(props);

    let providers = ['notifications', 'requests', 'messages'];
    let type = providers.indexOf(props.type) !== -1 ? props.type : 'notifications';

    this.state = {
      type,
      isData: !!props.events.length
    };
  }

  render() {
    const badge = this.state.isData ? <span className="badge bg-warning-400">n</span> : null;
    return (
      <li className="dropdown">
        <a href="/" className="dropdown-toggle" data-toggle="dropdown">
          <i className={icons[this.state.type]} />
          <span className="visible-xs-inline-block position-right">
            {polyglot.t(`social.${this.state.type}`)}
          </span>
          {badge}
        </a>

        <div className="dropdown-menu dropdown-content">
          <div className="dropdown-content-heading">
            {polyglot.t(`social.${this.state.type}`)}
            <ul className="icons-list">
              <li>
                <a href="/">
                  <i className="fa fa-info-circle fa-fw" />
                </a>
              </li>
            </ul>
          </div>

          <ul className="media-list dropdown-content-body width-300">
            <li className="media">
              <div className="media-left">
                <img src="/" className="img-circle img-sm" alt="" />
              </div>
              <div className="media-body">
                <a href="/" className="media-heading text-semibold">
                  Evgenii Torgonin
                </a>
                <span className="display-block text-muted text-size-small">Lead Developer</span>
              </div>
              <div className="media-right media-middle">
                <span className="status-mark border-success" />
              </div>
            </li>
          </ul>
        </div>
      </li>
    );
  }
}

export default NotifyDropdown;
