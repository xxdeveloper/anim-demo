import React, { PureComponent } from 'react';

import Brand from './Brand';
import MobileHeader from './MobileHeader';
import Search from './Search';
import RightPanel from './RightPanel';

class Header extends PureComponent {
  render() {
    return (
      <header className="navbar navbar-inverse navbar-fixed-top noselect">
        <div className="navbar-header">
          <Brand />
          <MobileHeader targetMenu="#navbar-mobile" />
        </div>

        <div className="navbar-collapse collapse" id="navbar-mobile">
          <Search onSubmit={() => alert('Dummy search ...')} />
          <RightPanel user={{name: "John Doe"}} onLogout={()=>{alert("This is demo")}} />
        </div>
      </header>
    );
  }
}

export default Header;
