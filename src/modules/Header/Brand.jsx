import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class Brand extends PureComponent {
  render() {
    return (
      <Link className="navbar-brand" to="/">
        <h3 className="no-margin">SHOWCASE</h3>
      </Link>
    );
  }
}

export default Brand;
