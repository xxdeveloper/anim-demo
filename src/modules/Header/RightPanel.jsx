import React, { PureComponent } from 'react';
import NotifyDropdown from './components/NotifyDropdown';
import UserMenu from './UserMenu';

class RightPanel extends PureComponent {
  render() {
    const { user, onLogout } = this.props;
    const menus = [
      <NotifyDropdown key="1" type="notifications" events={[]} />,
      <NotifyDropdown key="2" type="requests" events={[]} />,
      <NotifyDropdown key="3" type="messages" events={[]} />,
      <UserMenu key="4" user={user} onLogout={onLogout} />
    ];
    return (
      <ul className="nav navbar-nav navbar-right">
        {menus}
      </ul>
    );
  }
}

export default RightPanel;
