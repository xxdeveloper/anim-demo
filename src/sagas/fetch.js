import { call, put, take, fork, cancelled } from 'redux-saga/effects';
import api from '../api';
import { FETCH, fetch } from '../actions';
import createHistory from 'history/createBrowserHistory';

const history = createHistory();

function redirectTo(location) {
  history.push(location);
}

export function* helloSaga() {
  yield console.log('Welcome to Demo SPA!');
}

function* fetchUrlFlow(url) {
  let data = null;
  try {
    data = yield call(api.search.fetchUrl, url);
    yield put(fetch.urlReceive(data.data));
  } catch ({ response }) {
    yield put(fetch.urlReceive(response.data));
  } finally {
    if (yield cancelled()) {
        redirectTo('/');
    }
  }

  return data;
}

export function* fetchWatcher() {
  while (true) {
    const { url } = yield take(FETCH.URL_REQUEST);
    const task = yield fork(fetchUrlFlow, url);
  }
}
