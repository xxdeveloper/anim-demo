import { call, put, take, fork } from 'redux-saga/effects';
import { STREAMS, streams } from '../actions';
import api from '../api';

// Extract array data from server response
const toList = response => response.data === undefined || response.data.length === 0 ? [] : response.data;

function* getStreamsFlow(id) {
  try {
    if (!id) id = 'twitch-top';
    let response = yield call(api.streams.getTop, id);
    let payload = toList(response);
    yield put(streams.receiveVideosList(payload.streams));
  } catch ({ response }) {
    // if (response) yield put({ type: 'STREATS_GET_FAILED', payload: response });
    // else yield put({ type: 'CONNECTION_FAILED' });
    console.log('Can\'t load a video streams');
  } finally {
  }
  return true;
}

export function* streamsWatcher() {
  while (true) {
    const { category } = yield take(STREAMS.VIDEOS_REQUEST);
    const task = yield fork(getStreamsFlow, category);
  }
}
