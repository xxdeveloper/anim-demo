import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import './assets/css/bootstrap.css';
import './assets/css/core.css';
import './assets/css/components.css';
import './assets/css/colors.css';
import './assets/css/hi-style.css';

// Legacy for my CSS Theme based on Bootstrap 3
window.$ = window.jQuery = require('jquery');
require('bootstrap');

const store = configureStore();

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('hi-app')
);

registerServiceWorker();
