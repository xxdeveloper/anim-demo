import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { connect } from 'react-redux';
import { initEnvironment } from './actions';
import Header from "./modules/Header";
import Player from "./modules/Feed/Player";
import { SidebarLayout } from "./modules/Layout";
import Sidebar from "./modules/Sidebar";
import Feed from "./modules/Feed";

class App extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    initEnvironment(dispatch);
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <SidebarLayout path="/" component={Sidebar}>
            <Route path="/" component={Feed} />
            <Player />
          </SidebarLayout>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = store => ({
  env: store.app
});

export default connect(mapStateToProps)(App);
