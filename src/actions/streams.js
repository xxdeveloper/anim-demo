import { action } from "./action";

export const STREAMS = {
  VIDEOS_REQUEST: "VIDEOS_REQUEST",
  VIDEOS_RECEIVE: "VIDEOS_RECEIVE",

  OPEN_PLAYER: "OPEN_PLAYER",
  CLOSE_PLAYER: "CLOSE_PLAYER",
};

export const streams = {
  requestVideosList: category => action(STREAMS.VIDEOS_REQUEST, { category }),
  receiveVideosList: streams => action(STREAMS.VIDEOS_RECEIVE, { streams }),

  openPlayer: (source, channel) => action(STREAMS.OPEN_PLAYER, { source, channel }),
  closePlayer: () => action(STREAMS.CLOSE_PLAYER)
};
