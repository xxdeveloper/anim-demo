import { action } from './action';

export const FETCH = {
  URL_REQUEST: 'FETCH_URL_REQUEST',
  URL_RECEIVE: 'FETCH_URL_RECEIVE'
};

export const fetch = {
  urlRequest: url => action(FETCH.URL_REQUEST, { url }),
  urlReceive: data => action(FETCH.URL_RECEIVE, { data })
};
