import { action } from "./action";

export const ENV = {
  INIT_DONE: "INIT_DONE",
  RESIZE: "ENV_RESIZE",
  MOBILE: "ENV_MOBILE"
};

const env = {
  initDone: () => action(ENV.INIT_DONE),
  changeIsMobile: isMobile => action(ENV.MOBILE, { isMobile }),
  changeWidthAndHeight: (width, height) => action(ENV.RESIZE, { width, height })
};

export function initEnvironment(dispatch) {
  const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  );
  if (isMobile) {
    document.body.style.overflow = "hidden";
  }

  Promise.all([
    dispatch(env.changeIsMobile(isMobile)),
    dispatch(env.changeWidthAndHeight(document.documentElement.clientWidth, document.documentElement.clientHeight ))
  ]).then(() => {
    dispatch(env.initDone());
  });

  window.onresize = () => {
    dispatch(env.changeWidthAndHeight(document.documentElement.clientWidth, document.documentElement.clientHeight));
  };
}
