'use strict';
const gulp = require('gulp');
const less = require('gulp-less');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const concatCss = require('gulp-concat-css');
const cleanCSS = require('gulp-clean-css');

gulp.task('core-less', function() {
  return (gulp
      .src([
        './resources/less/limitless/_main/bootstrap.less',
        './resources/less/limitless/_main/core.less',
        './resources/less/limitless/_main/components.less',
        './resources/less/limitless/_main/colors.less'
      ])
      .pipe(sourcemaps.init())
      .pipe(less())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./src/assets/css')) );
});

gulp.task('core-sass', function() {
  return (gulp
      .src([
        './resources/sass/edt-limitless/_main/bootstrap.scss',
        './resources/sass/edt-limitless/_main/core.scss',
        './resources/sass/edt-limitless/_main/components.scss',
        './resources/sass/edt-limitless/_main/colors.scss'
      ])
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./src/assets/css')) );
});

gulp.task('hi-sass', function() {
  return (gulp
      .src('./resources/sass/theme/theme.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(concatCss('hi-style.css'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./src/assets/css')) );
});

gulp.task('sass:watch', function() {
  gulp.watch('./resources/sass/edt-limitless/_main/*.scss', ['core-sass']);
  gulp.watch('./resources/sass/theme/**/*.scss', ['core-sass']);
});
